#ifndef _HUFFMAN_CODE_CPP_
#define _HUFFMAN_CODE_CPP_

#include "HuffmanCode.h"

// convert byte to character
#define byte2Char(val) ((unsigned char) ((val) % 256))

// convert character to ASCII
#define char2Int(ch) ((int) (ch) < 0 ? 128 + (((int) (ch)) & 127): (((int) (ch)) & 127))

#include <sstream>
#include <typeinfo>

int HuffmanCode::zip(char* inputFile, char* outputFile) {
    Reader* input = new Reader(inputFile);

    Writer* output = new Writer(outputFile);

    HCZHeader* header = new HCZHeader();

    int frequencies[256] = {0};

    int charCode = input->readByte();

    frequencies[charCode]++;

    while(input->isHasNext()) {
        charCode = input->readByte();
        frequencies[charCode]++;
    }

    INode* root = BuildTree(frequencies);

    HuffCodeMap codes;

    GenerateCodes(root, HuffCode(), codes);

    delete root;

    int totalBit = 0,
        totalChar = codes.size();

    int codeLength;

    for (HuffCodeMap::const_iterator it = codes.begin(); it != codes.end(); ++it)
    {
        cout << it->first << " ";
        codeLength = it->second.size();
        ostringstream code;
        for (int i = 0; i < codeLength; i++) {
            //output->writeBit(it->second.at(i));
            code << it->second.at(i);
            totalBit++;
        }
        // HCZ trigger error?
        // header->set(it->first, codeLength, const_cast<char*>(code.str().c_str()));
        cout << endl;
    }
    header->setBodySize(totalBit);
    header->setTotal(totalChar, totalBit);
    header->write(output);

    output->~Writer();

    return UN_IMPLEMENT;
}

int HuffmanCode::unzip(char* inputFile, char* outputFile) {
    Reader* input = new Reader(inputFile);

    Writer* output = new Writer(outputFile);

    HCZHeader* header = new HCZHeader();

    if (header->read(input)) {
        int charCode = input->readByte();

        char* code;

        int len = 4;

        header->get(byte2Char(charCode), len, code);

        output->writeByte(atoi(code));

        while(input->isHasNext()) {
            charCode = input->readByte();
            
            char* code;

            header->get(byte2Char(charCode), len, code);

            output->writeByte(atoi(code));
        }
        
    }

    output->~Writer();

    return UN_IMPLEMENT;
}


#endif
