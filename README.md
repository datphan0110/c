How to run
==========

assignment1
-----------

```
$ cd assignment1/
$ g++ -o main common.cpp assignment1.cpp main.cpp -Wno-write-strings && ./main

```

assignment2
-----------

```
$ cd assignment2/
$ make && ./huffman zip input.txt output.txt
$ make && ./huffman unzip output.txt unzip.txt

```