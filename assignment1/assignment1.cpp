//#include "common.h"

#ifndef _common_h_
#include "common.h"
#define _common_h_
#endif

//////////////////////////////////////////////////////////////
//
//			Some common functions
//
//////////////////////////////////////////////////////////////


digitList *reverseList(digitList* L) {
	if (L == NULL || L->length() <= 1) {
		return L;
	}
	digitList* _pHead = L;
	digitList* pTempHead = _pHead;
	digitList* pRestNodes = NULL;
	digitList* pNextNode = NULL;
	digitList* _pTail = _pHead;

	pRestNodes = _pHead->nextDigit;

	while (pRestNodes != NULL) {
		pNextNode = pRestNodes;
		pRestNodes = pRestNodes->nextDigit;
		pNextNode->nextDigit = pTempHead;
		pTempHead = pNextNode;
	}
	_pHead = pTempHead;
	_pTail->nextDigit = NULL;

	return _pHead;
}



digitList* fillZeros(digitList* L, int n) {
	if (n <= 0) {
		return L;
	}

	if (L->getNextDigit() == NULL) {
		L->nextDigit = new digitList();
		return fillZeros(L->getNextDigit(), n-1);
	}
	return fillZeros(L->getNextDigit(), n);
}


digitList *trimDigitList(digitList* L)
{
	if (L->getNextDigit() == NULL) {
		return L;
	}
	digitList* reverseL = reverseList(L);


	while(reverseL != NULL) {
		if (reverseL->getDigit() != 0) {
			break;
		}
		reverseL = reverseL->getNextDigit();
	}

	if (reverseL == NULL) {
		return new digitList();
	}

	return reverseList(reverseL);
}

digitList *multiplyDigitLists(Integer X, Integer Y)
{
	/*X = X.trimDigit();
	Y = Y.trimDigit();*/

	int xLen = X.length();
	int yLen = Y.length();

	if (xLen == 0 || yLen == 0) {
		return new digitList();
	}

	if (xLen < yLen) {
		return multiplyDigitLists(Y, X);
	}

	if (xLen != 1 && xLen % radix % 2 != 0) {
		fillZeros(X.digits, 1);
		xLen = X.length();
	}

	if (yLen < xLen) {
		fillZeros(Y.digits, xLen - yLen);
		yLen = Y.length();
	}

	int len = xLen > yLen ? xLen : yLen;
	
	int n = len/2;

	if (xLen == 1 && yLen == 1) {
		int t = X.digits->getDigit() * Y.digits->getDigit();

		if (t > 9) {
			return new digitList(abs(t) % radix, new digitList(t / radix, NULL));
		}

		return new digitList(abs(t) % radix, NULL);
	}

	Integer xLeft, xRight,
				yLeft, yRight;

	xLeft = X.leftDigits(n);
	xRight = X.rightDigits(n);

	yLeft = Y.leftDigits(n);
	yRight = Y.rightDigits(n);

	return (
				addDigitLists(0,
					addDigitLists(0,

						multiplyDigitLists(xLeft, yLeft),

						Integer(
							addDigitLists(0,
								(multiplyDigitLists(xRight, yLeft)),
								(multiplyDigitLists(xLeft, yRight))
							)
						).shift(n).digits

					),
					Integer(multiplyDigitLists(xRight, yRight)).shift(2*n).digits
				)
			);
}
digitList *digitize(char str[80])
{
	digitList*	originL;
	digitList*	L = 0;
	digitList*	node;

	int i, k;
	int powValue = 0;
	int factorValue = 0;
	int len = strlen(str);

	for(i = 0; i< len; i++)
	{

		if (str[i] == '^') {
			L = trimDigitList(L);
			originL = L;
			for (k = i+1; k < len; k ++) {
				powValue = powValue * radix + str[k] - '0';
			}
			// a^0 = 1
			if (powValue <= 0) {
				L = new digitList(1, NULL);
			}
			for (k = 0; k < powValue - 1; k++) {
				L = multiplyDigitLists(L, originL);
			}

			break;
		}

		if (str[i] == '!') {
			L = trimDigitList(L);

			// 0! = 1
			if (factorValue <= 0) {
				L = new digitList(1, NULL);
			} else {
				for (k = factorValue; k > 1; k--) {
					L = multiplyDigitLists(L, digitize(k-1));
				}
			}

			break;
		}

		
		
		if(str[i] < '0' || str[i] > '9')	break;

		factorValue = factorValue * radix + str[i] - '0';
		node = new digitList(str[i] - '0', L);
		L = node;
	}

	return L;

}


digitList *subDigitLists(int c, digitList* L1, digitList* L2)
{

	// required L1 > L2
	if (compareDigitLists(L1, L2) < 0) {
		return subDigitLists(c, L2, L1);
	}

	if ((L1 == NULL) && (L2 == NULL))	return digitize(c);
	else if(L1 == NULL)					return subDigitLists(c, L2, NULL);
	else if(L2 == NULL)					
	{
		
		int t = c + L1->getDigit();
		int c2 = t / radix;
		if (t < 0) {
			t += 10;
			c2 = -1;
		}

		return (new digitList(abs(t) % radix,
						subDigitLists( c2, L1->getNextDigit(), NULL)));
	}
	else
	{
		int t = c + L1->getDigit() - L2->getDigit();
		int c2 = t / radix;
		if (t < 0) {
			t += 10;
			c2 = -1;
		}

		return (new digitList(abs(t) % radix,
					subDigitLists( c2, L1->getNextDigit(), L2->getNextDigit())));
	}
}





Integer computeValue(int operatorNum)
{
	Integer L1, L2;
	L1 = operandArr[0];

	for(int i = 0; i<operatorNum; i++)
	{
		L2 = operandArr[i+1];
		switch(operatorArr[i])
		{
		case '+':
			L1 = L1 + L2;
			break;
		case '-':
			L1 = L1 - L2;
			break;
		case '*':
			L1 = L1 * L2;
			break;
		}
	}
	
	return L1;
}
///////////////////////////////////////////////////////////////////////////
//
//		Some methods of Integer class
//
////////////////////////////////////////////////////////////////////
Integer Integer::operator +(Integer L)
{
	digits = trimDigitList(digits);
	L.digits = trimDigitList(L.digits);

	if (sign < 0 && L.sign < 0) {
		return Integer(sign, addDigitLists(0, digits, L.digits)).trimDigit();
	} else if (sign != L.sign) {
		if (sign < 0) {
			return Integer(compareDigitLists(digits, L.digits) <= 0 ? 1 : -1,
								subDigitLists(0, digits, L.digits)).trimDigit();
		} else if ( L.sign < 0) {
			return Integer(compareDigitLists(digits, L.digits) < 0 ? -1 : 1,
								subDigitLists(0, digits, L.digits)).trimDigit();
		}
		
	}

	return Integer(sign, addDigitLists(0, digits, L.digits)).trimDigit();
}

Integer Integer::operator -(Integer L)
{
	digits = trimDigitList(digits);
	L.digits = trimDigitList(L.digits);

	if (sign < 0 && L.sign < 0) {
		return Integer(compareDigitLists(L.digits, digits) < 0 ? -1 : 1,
							subDigitLists(0, L.digits, digits)).trimDigit();
	} else if (sign != L.sign) {
		
		if (sign < 0) {
			return Integer(sign, addDigitLists(0, digits, L.digits)).trimDigit();
		} else if ( L.sign < 0) {
			return Integer(sign, addDigitLists(0, digits, L.digits)).trimDigit();
		}
	}

	return Integer(compareDigitLists(digits, L.digits) < 0 ? -1 : 1,
						subDigitLists(0, digits, L.digits)).trimDigit();
}

Integer Integer::leftDigits(int n)
{
	return Integer(digits->leftDigits(n));
}

Integer Integer::rightDigits(int n)
{
	return Integer(digits->rightDigits(n));
}

Integer	Integer::shift(int n)
{/*
	cout << digits->length();
	cout << digits->getNextDigit()->getDigit();
	return Integer(digits);*/
	// not move if 0
	if (n <= 0 || digits->length() <= 0 ||
		(digits->getNextDigit() == NULL && digits->getDigit() == 0)) {
		return Integer(digits);
	}
	return Integer(new digitList(0, shift(n-1).digits));
}

Integer Integer::operator *(Integer Y)
{
	return Integer(sign * Y.sign, multiplyDigitLists(*this, Y));
}
